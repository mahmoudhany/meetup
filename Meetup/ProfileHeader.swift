//
//  ProfileHeader.swift
//  Meetup
//
//  Created by Mahmoud on 5/19/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//
import UIKit

class ProfileHeader: UICollectionReusableView {
    
    @IBOutlet weak var profilePicture: RoundedImageView!
    
    @IBOutlet weak var eventCountView: EventsCountView!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var bioLabel: UILabel!
    
    override func awakeFromNib() {
        details.translatesAutoresizingMaskIntoConstraints = false
        details.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func configureHeader(user: User, header: ProfileHeader){
//        header.eventCountView.user = user
        header.username.text = "\(String(describing: user.firstName ?? "")) \(String(describing: user.lastName ?? ""))".capitalized
        if let city = user.city, let country = user.country{
            if city == "" || country == ""{
                header.city.text = "\(city)\(country)".capitalized
            }else{
                header.city.text = "\(city), \(country)".capitalized
            }
        }
        header.jobTitle.text = "\(String(describing: user.jobTitle ?? ""))"
        header.details.text = "\(String(describing: user.details ?? ""))".capitalized
        if header.details.text.isEmpty == false {
            header.bioLabel.text = "Bio:"
        }
        header.profilePicture.image = user.profileImage
    }
}

extension ProfileHeader: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        self.layoutIfNeeded()
    }
}

