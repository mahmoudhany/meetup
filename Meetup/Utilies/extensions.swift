//
//  extensions.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

extension UIView{
   
   func addConstraintsWithFormat(format: String, views: UIView...){
      
      var viewsDictionary = [String: UIView]()
      
      for (index, view) in views.enumerated(){
         let key = "v\(index)"
         
         view.translatesAutoresizingMaskIntoConstraints = false
         viewsDictionary[key] = view
      }
      
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
   }
}

extension UIColor{
   static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor{
      return UIColor(displayP3Red: red/255, green: green/255, blue: blue/255, alpha: 1)
   }
}



extension UIFont {
   func sizeOfString(string: String, constrainedToWidth width: Double) -> CGSize {
    return NSString(string: string).boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),options: NSStringDrawingOptions.usesLineFragmentOrigin,attributes: [kCTFontAttributeName as NSAttributedStringKey: self],context: nil).size
   }
}


extension Date{
    static func stringFromDate(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
}







