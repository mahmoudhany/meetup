//
//  Constants.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
//import SwiftKeychainWrapper

let BASEURL = "https://mhany-backend1.herokuapp.com/api"
let authURL = "\(BASEURL)/auth"
let loginUrl = URL(string: "\(authURL)/login")
let signupUrl = URL(string: "\(authURL)/signup")
let profileUrl = URL(string: "\(BASEURL)/profile")
let uploadUrl = "\(BASEURL)/upload"
let eventsUrl = URL(string: "\(BASEURL)/events")
let favoriteEvent = "https://mhany-backend1.herokuapp.com/api/profile/fav/add/"
let deleteFavoriteEvent = "https://mhany-backend1.herokuapp.com/api/profile/fav/delete/"
let logoutUrl = URL(string: "\(authURL)/logout")

let mainGreenColor = UIColor.rgb(red: 17, green: 210, blue: 195)



