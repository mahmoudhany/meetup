//
//  LoginViewController.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class LoginViewController: UITableViewController {
   
   var user = User()
   
   @IBOutlet weak var email: CustomTextField!
   @IBOutlet weak var password: CustomTextField!
   @IBOutlet weak var signin: UIButton!
   
   override func viewDidLoad() {
      
   }
   
   @IBAction func loginTapped(_ sender: UIButton) {
      guard let email = self.email.text, let password = self.password.text else{
         return
      }
      if Validation.isValidEmail(email: email) &&
         Validation.isValidPassword(password: password)
      {
         let parameters = [
            "email": email,
            "password": password
         ]
        ApiService.sharedInstance.makeAuth(url: loginUrl!, parameters: parameters, completion: { 
            if let _: String = KeychainWrapper.standard.string(forKey: "accessToken"){
               self.performSegue(withIdentifier: "toMainNav", sender: self)
            }else{
               let alert = Validation.validationAlert(title: "login failed", message: "Please check your username and passord", actionTitle: "Ok")
               self.present(alert, animated: true, completion: nil)
            }
            
         })
         
      }else{
         let alert = Validation.validationAlert(title: "login failed", message: "Please enter your name and passord", actionTitle: "Ok")
         self.present(alert, animated: true, completion: nil)
      }
      
   }
   
}






