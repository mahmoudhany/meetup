//
//  SignupViewController.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SignupViewController: UITableViewController, UINavigationControllerDelegate{
    
    // MARK: Outlets
    @IBOutlet weak var firstName: CustomTextField!
    @IBOutlet weak var lastName: CustomTextField!
    @IBOutlet weak var email: CustomTextField!
    @IBOutlet weak var password: CustomTextField!
    @IBOutlet weak var repeatPassword: CustomTextField!
    @IBOutlet weak var profileImageView: RoundedImageView!
    @IBOutlet weak var imageViewSuperView: UIView!
    
    // MARK: varibles
    var avatar: String? = ""
//    var selectedImage: UIImage?
    let selectImageButton = RoundedButton()
    lazy var imagePicker: UIImagePickerController = {
        let ip = UIImagePickerController()
        return ip
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDelegates()
        addButtonToImageView()
        selectImageButton.addTarget(self, action: #selector(pickProfileImage), for: .touchUpInside)
    }
    func initDelegates(){
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        password.delegate = self
    }
    func addButtonToImageView(){
        imageViewSuperView.addSubview(selectImageButton)
        imageViewSuperView.addConstraintsWithFormat(format: "H:[v0(25)]-2-|", views: selectImageButton)
        imageViewSuperView.addConstraintsWithFormat(format: "V:|-2-[v0(25)]", views: selectImageButton)
        profileImageView.isUserInteractionEnabled = true
    }
    
    @objc func pickProfileImage(_ sender: UIButton){
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
        
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func dismissSignup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createTapped(_ sender: Any) {
        guard
            let firstName = self.firstName.text,
            let lastName = self.lastName.text,
            let email = self.email.text,
            let password = self.password.text,
            let repeatPassword = self.repeatPassword.text,
            let avatar = self.avatar
            else{
                return
        }
        
        if Validation.isValidName(name: firstName) &&
            Validation.isValidName(name: lastName) &&
            Validation.isValidEmail(email: email) &&
            Validation.isValidPassword(password: password) &&
            Validation.isValidPassword(password: repeatPassword) &&
            repeatPassword == password{
            let parameters = [
                "firstname": firstName,
                "lastname": lastName,
                "email": email,
                "password": password,
                "avatar": avatar
            ]
            
            ApiService.sharedInstance.makeAuth(url: signupUrl!, parameters: parameters, completion: {
                
                if let accessToken: String = KeychainWrapper.standard.string(forKey: "accessToken"){
                    print(accessToken)
                    self.performSegue(withIdentifier: "toMainNav", sender: self)
                }else{
                    let alert = Validation.validationAlert(title: "signup failed", message: "Please check your username and password", actionTitle: "Ok")
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
        }else{
            let alert = Validation.validationAlert(title: "Error", message: "Please fill all Fields correctly", actionTitle: "Dismiss")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backToSignin(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: textField handling
extension SignupViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstName:
            lastName.becomeFirstResponder()
        case lastName:
            email.becomeFirstResponder()
        case email:
            password.becomeFirstResponder()
        case password:
            repeatPassword.becomeFirstResponder()
        default:
            firstName.resignFirstResponder()
        }
        return true
    }
    
    
}

// MARK: imagePicker handling
extension SignupViewController: UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            profileImageView.image  = selectedImage
            ApiService.sharedInstance.uploadImage(image: selectedImage, headers: nil, completion: { (imageUrl) in
                self.avatar = imageUrl
            })
            self.dismiss(animated: true, completion: nil)
            
        }else{
            print("Something went wrong")
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}













