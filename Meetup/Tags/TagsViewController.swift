//
//  TagsViewController.swift
//  Meetup
//
//  Created by Mahmoud on 7/24/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class TagsViewController: UIViewController{
    @IBOutlet weak var addTagsButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: FlowLayout!
    
    let tagCell = "TagCell"
    let tagsArray = ["Technology","HR","Marketing","Social Media","Blockchain"]
    var selectedTagsTitles: [String] = []
    var sizingCell: TagCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNib()
        configureCollectionView()
        addTagsButton.isEnabled = false
    }
    private func setupNib(){
        let cellNib = UINib(nibName: tagCell, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: tagCell)
        self.sizingCell = (cellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
        self.flowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8)
    }
    private func configureCollectionView(){
        self.collectionView.backgroundColor = UIColor.clear
        collectionView.allowsMultipleSelection = true
    }
    @IBAction func dismissTagsVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: UICollectionViewDelegate
extension TagsViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TagCell
        cell.tagLabel.textColor = .black
        cell.backgroundColor = UIColor.rgb(red: 216, green: 216, blue: 216)
        
        selectedTagsTitles = selectedTagsTitles.filter{$0 != cell.tagLabel.text}
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TagCell
        cell.tagLabel.textColor = .white
        cell.backgroundColor = mainGreenColor
        selectedTagsTitles.append(cell.tagLabel.text!)
        if selectedTagsTitles.count > 0{
            addTagsButton.isEnabled = true
        }
    }
    
}
// MARK: UICollectionViewDataSource
extension TagsViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: tagCell, for: indexPath) as! TagCell
        self.configureCell(cell: cell, forIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: TagCell, forIndexPath indexPath: IndexPath) {
        let tag = tagsArray[indexPath.row]
        cell.tagLabel.text = tag
    }
    
    
}
// MARK: UICollectionViewDelegateFlowLayout
extension TagsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.configureCell(cell: self.sizingCell!, forIndexPath: indexPath)
        
        return self.sizingCell!.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
    }
}











