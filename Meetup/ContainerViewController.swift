//
//  ContainerViewController.swift
//  Meetup
//
//  Created by Mahmoud on 9/12/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    enum SlideOutState {
        case collapsed
        case leftPanelExpanded
        //    case rightPanelExpanded
    }
    
    var timelineNavigationController: UINavigationController!
    var timelineViewController: TimelineViewController!
    
    var currentState: SlideOutState = .collapsed
    var sideMenuViewController: SideMenuController?
    
    let timeLineExpandedOffset: CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timelineViewController = UIStoryboard.timelineViewController()
        timelineViewController.delegate = self

        timelineNavigationController = UINavigationController(rootViewController: timelineViewController)
        view.addSubview(timelineNavigationController.view)
        addChildViewController(timelineNavigationController)
        
        timelineNavigationController.didMove(toParentViewController: self)
    }
}

extension ContainerViewController: TimelineViewControllerDelegate{
    
    func toggleSideMenu(){
        let notAleardyExpanded = (currentState != .leftPanelExpanded)
        if notAleardyExpanded{
            addLeftPanelViewController()
        }
        animateLeftPanel(shouldExpand: notAleardyExpanded)
        
    }

    func addLeftPanelViewController() {
        guard sideMenuViewController == nil else{ return }
        if let vc = UIStoryboard.leftViewController(){
            addChildSidePanelController(vc)
            sideMenuViewController = vc
        }
        
    }
    func addChildSidePanelController(_ sideMenuController: SideMenuController){
        sideMenuController.delegate = timelineViewController
        view.insertSubview(sideMenuController.view, at: 0)
        addChildViewController(sideMenuController)
        sideMenuController.didMove(toParentViewController: self)
    }

    func animateLeftPanel(shouldExpand: Bool) {
        
        if shouldExpand {
            currentState = .leftPanelExpanded
            animateCenterPanelXPosition(
                targetPosition: timelineNavigationController.view.frame.width - timeLineExpandedOffset)
            
        } else {
            animateCenterPanelXPosition(targetPosition: 0) { finished in
                self.currentState = .collapsed
                self.sideMenuViewController?.view.removeFromSuperview()
                self.sideMenuViewController = nil
            }
        }
    }
    
    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)? = nil) {
        
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.timelineNavigationController.view.frame.origin.x = targetPosition
        }, completion: completion)
    }

    func collapseSideMenu() {
        
        switch currentState {
        case .leftPanelExpanded:
            toggleSideMenu()
        default:
            break
        }
    }
}
    
private extension UIStoryboard {
    
    static func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    static func leftViewController() -> SideMenuController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "SideMenuController") as? SideMenuController
    }
    
    static func timelineViewController() -> TimelineViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "TimelineViewController") as? TimelineViewController
    }
}
    
   

