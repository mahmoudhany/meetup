//
//  ProfileViewController.swift
//  Meetup
//
//  Created by Mahmoud on 4/29/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import SDWebImage

protocol ProfileControllerDelegate {
    func getFavEvents(events: [Event])
}

class ProfileViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, ProfileEventCellDelegate{
    
    var user: User?
    var favoriteEvents = [Event]()
    fileprivate let cellId = "CellId"
    let timelineViewController = TimelineViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor =  UIColor.rgb(red: 213, green: 213, blue: 213)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProfile()
    }
    func getProfile(){
        if let accessToken = KeychainWrapper.standard.string(forKey: "accessToken"){
            let headers = [
                "x-access-token": accessToken,
                "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
            ]
            ApiService.sharedInstance.fetchProfile(url: profileUrl!, headers: headers, method: .get, parameters: nil, completion: { (user) in
                self.user = user
                self.collectionView?.reloadData()
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditProfile"{
            if let editProfile = segue.destination as? EditProfileViewController{
                editProfile.user = self.user
            }
        }
    }
    
}

// MARK: UICollectionViewDateSource
extension ProfileViewController{
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.favoriteEvents.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventCell
        cell.delegate = self
        // Configure the cell
        cell.eventName.text = self.favoriteEvents[indexPath.row].name
        cell.eventLocation.text = self.favoriteEvents[indexPath.row].address
        cell.eventTime.text = self.favoriteEvents[indexPath.row].time
        
        return cell
    }
    
    func didTapFavoriteButton(cell: EventCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else{return}
        guard let id = self.favoriteEvents[indexPath.row].id else{return}

        guard let accessToken = KeychainWrapper.standard.string(forKey: "accessToken") else{return}
        let headers = [
            "x-access-token": accessToken,
            "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
        ]
        ApiService.sharedInstance.deleteFavEvent(id: id, headers: headers) { (isFavorited) in
            self.favoriteEvents.remove(at: indexPath.row)
            self.collectionView?.reloadData()
        }
    }
    
    // header
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "profileHeader", for: indexPath) as! ProfileHeader
        
        DispatchQueue.main.async {
            if let user = self.user{
                header.configureHeader(user: user, header: header)
                self.collectionView?.reloadData()
            }
        }
        return header
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.reloadData()
    }
    
}

// MARK: UICollectionViewDelegate
extension ProfileViewController{
    //cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 80)
    }
    
}





