//
//  SideMenu.swift
//  Meetup
//
//  Created by Mahmoud on 6/16/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//


import UIKit
import SwiftKeychainWrapper

class SideMenuController: UITableViewController {
    let timeLineController = TimelineViewController()
    let cellId = "menuCell"
    var delegate: SideMenuControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}


    // MARK: - Table view data source
extension SideMenuController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuCell
        let titles = ["Profile", "Log out"]
        
        cell.nameLabel.text = titles[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        guard let title = cell.nameLabel.text else{ return }
        if title != "Log out"{
            print("selected")
            delegate?.didSelectCell(cell)
        }else{
            
            if let accessToken = KeychainWrapper.standard.string(forKey: "accessToken"){
                let headers = [
                    "x-access-token": accessToken,
                    "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
                ]
                ApiService.sharedInstance.logout(url: logoutUrl!, headers: headers, completion: {
                    KeychainWrapper.standard.removeObject(forKey: "accessToken")
                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    appDelegate.window?.rootViewController = loginVC
                })
            }
            
        }
    }
    
    
    
    
}
