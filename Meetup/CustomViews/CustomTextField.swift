//
//  CustomTextField.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate {
    
    let textFieldBorder: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        return view
    }()
    
    func setupTextField(){
        self.addSubview(textFieldBorder)
        self.addConstraintsWithFormat(format: "H:|[v0]|", views: textFieldBorder)
        self.addConstraintsWithFormat(format: "V:[v0(1)]|", views: textFieldBorder)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
        setupTextField()
    }
    
    
}
