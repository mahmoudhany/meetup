//
//  RoundedImageView.swift
//  Meetup
//
//  Created by Mahmoud on 4/22/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
class RoundedImageView: UIImageView {
   
   override init(frame: CGRect) {
      super.init(frame: frame)
   }
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      
      self.layer.cornerRadius = self.frame.size.height / 2
      self.clipsToBounds = true
   }
   
}











