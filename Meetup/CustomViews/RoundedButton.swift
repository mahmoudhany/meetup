//
//  RoundedButton.swift
//  Meetup
//
//  Created by Mahmoud on 4/23/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupButton(){
        let title = UILabel()
        title.text = "+"
        title.font = UIFont.systemFont(ofSize: 18)
        title.textColor = .white
        self.backgroundColor = mainGreenColor
        self.layer.cornerRadius = 12.5
        self.setTitle(title.text, for: .normal)
        self.clipsToBounds = false
    }
    
}
