//
//  SettingCell.swift
//  Meetup
//
//  Created by Mahmoud on 6/16/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SourceSansPro-Light", size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let border: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
//    override var isHighlighted: Bool{
//        didSet{
//            backgroundColor = isHighlighted ? UIColor.rgb(red: 246, green: 246, blue: 246): .white
//            nameLabel.textColor = isHighlighted ? .black : .black
//        }
//    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        addSubview(nameLabel)
        addSubview(border)
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
            ])
        
        NSLayoutConstraint.activate([
            border.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            border.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            border.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            border.heightAnchor.constraint(equalToConstant: 0.5)
            ])
    }
    
}
