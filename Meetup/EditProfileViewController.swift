//
//  EditProfileViewController.swift
//  Meetup
//
//  Created by Mahmoud on 5/21/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import SDWebImage
import Alamofire

class EditProfileViewController: UITableViewController, UINavigationControllerDelegate{
    
    // MARK: Outlets
    @IBOutlet weak var firstName: CustomTextField!
    @IBOutlet weak var lastName: CustomTextField!
    @IBOutlet weak var email: CustomTextField!
    @IBOutlet weak var city: CustomTextField!
    @IBOutlet weak var country: CustomTextField!
    @IBOutlet weak var company: CustomTextField!
    @IBOutlet weak var jobTitle: CustomTextField!
    @IBOutlet weak var bio: CustomTextField!
    
    @IBOutlet weak var profileImageView: RoundedImageView!
    @IBOutlet weak var imageViewSuperView: UIView!
    
    var user: User?
    let selectImageButton = RoundedButton()
    var localPath: String?
    var headers: HTTPHeaders?
    var imageUrl: String?
    lazy var imagePicker: UIImagePickerController = {
        let ip = UIImagePickerController()
        return ip
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        checkLogin()
        loadUserData()
        addButtonToImageView()
        
        selectImageButton.addTarget(self, action: #selector(pickProfileImage), for: .touchUpInside)

    }
    func checkLogin(){
        guard let accessToken = KeychainWrapper.standard.string(forKey: "accessToken") else{
            return
        }
        headers = [
            "x-access-token": accessToken,
            "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
        ]
    }
    func addButtonToImageView(){
        imageViewSuperView.addSubview(selectImageButton)
        imageViewSuperView.addConstraintsWithFormat(format: "H:[v0(25)]-2-|", views: selectImageButton)
        imageViewSuperView.addConstraintsWithFormat(format: "V:|-2-[v0(25)]", views: selectImageButton)
        profileImageView.isUserInteractionEnabled = true
    }
    private func initDelegates(){
        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        country.delegate = self
        company.delegate = self
        city.delegate = self
        jobTitle.delegate = self
        bio.delegate = self
    }
    
    func loadUserData(){
        if let user = self.user{
            self.firstName.text = user.firstName
            self.lastName.text = user.lastName
            self.email.text = user.email
            self.city.text = user.city
            self.country.text = user.country
            self.company.text = user.company
            self.bio.text = user.details
            self.jobTitle.text = user.jobTitle
            if user.avatar != nil{
                self.imageUrl = user.avatar
            }else{
                self.imageUrl = ""
            }
            self.profileImageView.image = user.profileImage
        }
    }
    
    @IBAction func dismissEditProfile(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        
        guard
            let firstName = self.firstName.text,
            let lastName = self.lastName.text,
            let city = self.city.text,
            let country = self.country.text,
            let company = self.company.text,
            let jobTitle = self.jobTitle.text,
            let bio = self.bio.text,
            let avatar = self.imageUrl
            else{
                return
        }
        
        if Validation.isValidName(name: firstName) &&
            Validation.isValidName(name: lastName)
        {
            let parameters = [
                "firstname": firstName,
                "lastname": lastName,
                //            "email": email,
                "city": city,
                "country": country,
                "company": company,
                "details": bio,
                "job_title": jobTitle,
                "avatar": avatar
            ]
            
            // submit new data
            ApiService.sharedInstance.fetchProfile(url: profileUrl!, headers: headers, method: .put, parameters: parameters, completion: { (user) in
                
                self.dismiss(animated: true, completion: nil)
                
            })
            
        }else{
            let alert = Validation.validationAlert(title: "Failed", message: "Please fill all Fields correctly", actionTitle: "Dismiss")
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: textField handling
extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstName:
            lastName.becomeFirstResponder()
        case lastName:
            city.becomeFirstResponder()
        case city:
            country.becomeFirstResponder()
        case country:
            company.becomeFirstResponder()
        case company:
            jobTitle.becomeFirstResponder()
        default:
            firstName.resignFirstResponder()
        }
        return true
    }
    
}

// MARK: imagePicker handling
extension EditProfileViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else{
            return
        }
        profileImageView.image  = selectedImage
        
        ApiService.sharedInstance.uploadImage(image: selectedImage, headers: headers!) { urlString in
            self.imageUrl = urlString
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func pickProfileImage(_ sender: UIButton){
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
}

