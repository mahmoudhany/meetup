//
//  Event.swift
//  Meetup
//
//  Created by Mahmoud on 7/2/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class EventsApiResponse: Mappable{
    var events: [Event]?
    var success: Bool?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        events <- map["events"]
        success <- map["success"]
    }
}

class Event: Mappable{
    var id: String?
    var name: String?
    var country: String?
    var city: String?
    var address: String?
    var date: Date?
    var time: String?
    var allowedNumber: Int?
    var tags: [String: String] = [:]
    var details: String?
    var owner: User?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id            <- map["_id"]
        name          <- map["name"]
        country       <- map["country"]
        city          <- map["city"]
        address       <- map["address"]
        time          <- map["time"]
        allowedNumber <- map["allowed_umber"]
        tags          <- map["tags"]
        details       <- map["details"]
        owner         <- map["_owner"]
        date          <- (map["date"], DateFormatTransform(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ"))
    }
    
}

public class DateFormatTransform: TransformType {
    
    public typealias Object = Date
    public typealias JSON = String
    var dateFormatter = DateFormatter()
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormatter.dateFormat = dateFormat
    }
    
    public func transformFromJSON(_ value: Any?) -> Date? {
        if let dateString = value as? String {
            return self.dateFormatter.date(from: dateString)
        }
        return nil
    }
    public func transformToJSON(_ value: Date?) -> JSON? {
        if let date = value {
            
            return self.dateFormatter.string(from: date)
        }
        return nil
    }
}




