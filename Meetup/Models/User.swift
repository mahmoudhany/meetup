//
//  User.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class User{
   
   var email: String?
   var firstName: String?
   var lastName: String?
   var attendEvents: [Dictionary<String, Any>]?
   var favoriteEvents: [Dictionary<String, String>]?
   var interests: [Dictionary<String, Any>]?
   var avatar: String?
   var profileImage: UIImage? = UIImage(named: "profilePlaceholder")
   var city: String?
   var country: String?
   var company: String?
   var details: String?
   var jobTitle: String?
   var fbLink: String?
   var linkedinLink: String?
}
