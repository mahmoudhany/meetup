//
//  EventsCountView.swift
//  Meetup
//
//  Created by Mahmoud on 5/6/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class EventsCountView: UIView {
    
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.isScrollEnabled = false
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    func setupViews(){
        addSubview(collectionView)
        collectionView.register(EventsCountCell.self, forCellWithReuseIdentifier: cellId)
        addConstraintsWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let selectedIndex = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndex, animated: true, scrollPosition: .centeredVertically)
        setupViews()
    }
    
}

// MARK: UICollectionViewDataSource
extension EventsCountView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! EventsCountCell
        
        if indexPath.item == 0{
            cell.icon.image = UIImage(named: "check")?.withRenderingMode(.alwaysTemplate)
            cell.label.text = "6"
            
        }else if indexPath.item == 1{
            cell.icon.image = UIImage(named: "clock")?.withRenderingMode(.alwaysTemplate)
            cell.label.text = "4"
        }else{
            cell.icon.image = UIImage(named: "attention")?.withRenderingMode(.alwaysTemplate)
            cell.label.text = "10"
        }
        return cell
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension EventsCountView: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = frame.width / 3
        return CGSize(width: width, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}











