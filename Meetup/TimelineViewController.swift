//
//  TimelineViewController.swift
//  Meetup
//
//  Created by Mahmoud on 6/29/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper


protocol TimelineControllerDelegate {
    func checkFavoritedEvents()
}

class TimelineViewController: UITableViewController, EventCellDelegate{
    
    var user: User?
    
    var eventsArray = [Event]()
    var sectionsTitle = [String]()
    var sectionData = [String: [Event]]()
    
    public var favoriteEvents = [Event]()
    private let cellId = "cell"
    
    var delegate: TimelineViewControllerDelegate?
    
    var addEventController = AddEventViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoriteEvents.removeAll()
        sectionsTitle.removeAll()
        eventsArray.removeAll()
        sectionData.removeAll()
        getProfile()
        getEvents()

    }
    func getProfile(){
        if let accessToken = KeychainWrapper.standard.string(forKey: "accessToken"){
            let headers = [
                "x-access-token": accessToken,
                "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
            ]
            ApiService.sharedInstance.fetchProfile(url: profileUrl!, headers: headers, method: .get, parameters: nil, completion: { (user) in
                self.user = user
            })
        }
    }
    
    private func getEvents(){
        if let accessToken = KeychainWrapper.standard.string(forKey: "accessToken"){
            let headers = [
                "x-access-token": accessToken,
                "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
            ]
            ApiService.sharedInstance.getEvents(url: eventsUrl!, headers: headers, method: .get, parameters: nil, completion: { events in
                if let events = events{
                    DispatchQueue.main.async {
                        events.forEach({ (event) in
                            self.handleSectionArrays(event: event)
                        })
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    private func handleSectionArrays(event: Event){
        guard let date = event.date else{ return }
        let dateFromString = Date.stringFromDate(date: date)
        
        if var sectionValues = self.sectionData[dateFromString] {
            sectionValues.append(event)
            self.sectionData[dateFromString] = sectionValues
        } else {
            self.sectionData[dateFromString] = [event]
        }
        self.eventsArray.append(event)
        self.sectionsTitle.append(dateFromString)
        
        // sort by Date
        self.sectionsTitle = [String](self.sectionData.keys)
        self.sectionsTitle = self.sectionsTitle.sorted(by: { $0 < $1 })
    }
    
    @IBAction func openSideMenu(_ sender: Any) {
        delegate?.toggleSideMenu?()
    }
}

// MARK: - Table view data source
extension TimelineViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsTitle.count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionsTitle[section]
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionKey = sectionsTitle[section]
        
        if let sectionValues = sectionData[sectionKey] {
            return sectionValues.count
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EventTableViewCell
        cell.delegate = self
        self.configureCell(cell: cell, forIndexPath: indexPath)
        return cell
    }
    private func configureCell(cell: EventTableViewCell, forIndexPath indexPath: IndexPath){
        let sectionKey = sectionsTitle[indexPath.section]
        
        if let sectionValues = sectionData[sectionKey] {
            let id = sectionValues[indexPath.row].id
            
            cell.eventName.text = sectionValues[indexPath.row].name
            if let time = sectionValues[indexPath.row].time{
                cell.eventTime.text = time
            }
            cell.eventLocation.text = sectionValues[indexPath.row].address
            
            guard let isInFavorite = user?.favoriteEvents?.contains(where: { (dict) -> Bool in
                return id == dict["_id"]
            })else{return}
            
            cell.isFavorited = isInFavorite
            cell.accessoryView?.tintColor = cell.isFavorited ? mainGreenColor: .black
            
            if cell.isFavorited == true{

                self.favoriteEvents.append(sectionValues[indexPath.row])
            }
            else{
                guard let index = self.favoriteEvents.index(where: { (event) -> Bool in
                    return id == event.id
                })else{return}
                self.favoriteEvents.remove(at: index)
            }
            
            //remove border for last item
            let count = sectionValues.count - 1
            if indexPath.row == count{
                cell.border.isHidden = true
            }
        }
        
    }
    
    
    func didTapFavoriteButton(cell: EventTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else{return}
        
        let sectionKey = sectionsTitle[indexPath.section]
        let section = self.sectionData[sectionKey] ?? []
//        let selectedEvent = section[indexPath.row]
        guard let id = section[indexPath.row].id else{return}
        
        guard let accessToken = KeychainWrapper.standard.string(forKey: "accessToken") else{return}
        let headers = [
            "x-access-token": accessToken,
            "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
        ]
        if cell.isFavorited == false{
            ApiService.sharedInstance.addFavEvent(id: id, headers: headers) { (isFavorited) in
                cell.isFavorited = true
                cell.accessoryView?.tintColor = mainGreenColor
            }
        }else{
            ApiService.sharedInstance.deleteFavEvent(id: id, headers: headers) { (isFavorited) in
                cell.isFavorited = false
                cell.accessoryView?.tintColor = .black
            }
        }
    }
    
}


extension TimelineViewController: SideMenuControllerDelegate {
    func didSelectCell(_ cell: MenuCell) {
        delegate?.collapseSideMenu?()
        
        performSegue(withIdentifier: "toProfile", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProfile"{
            let distination = segue.destination as! ProfileViewController
            distination.favoriteEvents = self.favoriteEvents
            print(self.favoriteEvents)
        }
    }
}


