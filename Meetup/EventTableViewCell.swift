//
//  EventTableViewCell.swift
//  Meetup
//
//  Created by Mahmoud on 6/29/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

protocol EventCellDelegate{
    func didTapFavoriteButton(cell: EventTableViewCell)
}

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventColorView: UIView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var timeIcon: UIImageView!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var eventLocation: UILabel!
    
    var isFavorited: Bool!
    
    var delegate: EventCellDelegate?
    let border: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.addSubview(border)
        border.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            border.heightAnchor.constraint(equalToConstant: 1),
            border.leftAnchor.constraint(equalTo: eventColorView.rightAnchor, constant: 10),
            border.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            border.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
            ])
        
        let favoriteButton = UIButton(type: .system)
        favoriteButton.setImage(#imageLiteral(resourceName: "favorite-fill").withRenderingMode(.alwaysTemplate), for: .normal)
        favoriteButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        favoriteButton.addTarget(self, action: #selector(markAsFavorite), for: .touchUpInside)
        accessoryView = favoriteButton
        
    }
    @objc func markAsFavorite(){
        delegate?.didTapFavoriteButton(cell: self)
    }
    
    
}
