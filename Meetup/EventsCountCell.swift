//
//  EventsCountCell.swift
//  Meetup
//
//  Created by Mahmoud on 5/6/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class EventsCountCell: UICollectionViewCell {
   
   let label: UILabel = {
      let label = UILabel()
      label.font = UIFont(name: "SourceSansPro-Bold.ttf", size: 10)
//      label.text = "32"
      return label
   }()
   let icon: UIImageView = {
      let iv = UIImageView()
      iv.image = UIImage(named: "edit")?.withRenderingMode(.alwaysTemplate)
      iv.contentMode = .scaleAspectFit
      iv.tintColor = .gray
      return iv
   }()
    
   override var isSelected: Bool{
      didSet{
         if isSelected {
            backgroundColor = mainGreenColor
            icon.tintColor = .white
            label.textColor = .white
         }else{
            backgroundColor = .clear
            icon.tintColor = .gray
            label.textColor = .black
            
         }
      }
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      setupViews()
   }
   func setupViews(){
      addSubview(label)
      addSubview(icon)
      
      addConstraintsWithFormat(format: "V:|-16-[v0(28)]-8-[v1]", views: icon, label)
      addConstraint(NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
      addConstraint(NSLayoutConstraint(item: icon, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
   }
   
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
}
