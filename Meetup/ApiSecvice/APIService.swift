//
//  APIService.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftKeychainWrapper
import ObjectMapper


class ApiService: NSObject{
    
    let loginViewcontroller = LoginViewController()
    //   let signupViewController = SignupViewController()
    var alert: UIAlertController?
    static var sharedInstance = ApiService()
    private let apiKey = ["api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"]
    
    func makeAuth(url: URL, parameters: [String: Any], completion: @escaping () -> ()){
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: apiKey).responseJSON { (response) in
            switch response.result{
            case .success:
                if let result = response.value as? NSDictionary{
                    if let success = result["success"] as? Bool{
                        if success {
                            if let accessToken = result["token"] as? String{
                                
                                let saveAccessToken: Bool = KeychainWrapper.standard.set(accessToken, forKey: "accessToken")
                                print(saveAccessToken)
                            }
                        }else{
                            if let errorMessage = result["message"] as? String{
                                print(errorMessage)
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        completion()
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func logout(url: URL, headers: HTTPHeaders,completion: @escaping () -> ()){
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result{
            case .success:
                if let result = response.value as? NSDictionary{
                    if let success = result["success"] as? Bool{
                        if success {
                            print(result)
                        }else{
                            if let errorMessage = result["message"] as? String{
                                print(errorMessage)
                            }
                        }
                    }
                    completion()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func fetchProfile(url: URL, headers: HTTPHeaders?, method: HTTPMethod?, parameters: [String: Any]?, completion: @escaping (User?) -> ()){
        
        if method == .put{
            Alamofire.request(url, method: method!, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (response) in
                if let result = response.result.value{
                    print(result)
                }
            })
        }
        
        Alamofire.request(url, method: method!, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                if let result = response.result.value as? NSDictionary{
                    // check webservice response
                    if let success = result["success"] as? Bool{
                        // check token validation
                        if success {
                            // TODO: parse response to User
                            
                            let user = User()
                            if let profile = result["profile"] as? NSDictionary{
                                user.firstName = profile["firstname"] as? String
                                user.lastName = profile["lastname"] as? String
                                user.email = profile["email"] as? String
                                user.jobTitle = profile["job_title"] as? String
                                user.avatar = profile["avatar"] as? String
                                
                                if let avatar = profile["avatar"] as? String{
                                    Alamofire.request(avatar).responseImage { response in
                                        
                                        if let image = response.result.value{
                                            user.profileImage = image
                                        }
                                    }
                                }
                                user.city = profile["city"] as? String
                                user.country = profile["country"] as? String
                                user.company = profile["company"] as? String
                                user.details = profile["details"] as? String
                                user.fbLink = profile["facebook_link"] as? String
                                user.linkedinLink = profile["linkedin_link"] as? String
                                user.favoriteEvents = profile["favoriteEvents"] as? [Dictionary<String, String>]
                                user.attendEvents = profile["attendeEvents"] as? [Dictionary<String, Any>]
                                user.interests = profile["interests"] as? [Dictionary<String, Any>]
                            }
                            // TODO: use it to fix alert apperince
                            completion(user)
                            
                        }else{
                            
                            if let errorMessage = result["message"] as? String{
                                print(errorMessage)
                            }
                            // MARK: MVCing this part
                            KeychainWrapper.standard.removeObject(forKey: "accessToken")
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let defaultPage = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            let appDelegate = UIApplication.shared.delegate!
                            
                            appDelegate.window??.rootViewController = defaultPage
                        }
                    }
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // image upload
    func uploadImage(image: UIImage, headers: HTTPHeaders?, completion: @escaping (String)->()){
        
        guard let imageData = UIImageJPEGRepresentation(image, 0.5) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            
        }, to: uploadUrl, headers: headers, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress { progress in
                    print(progress)
                    //                  progressCompletion(Float(progress.fractionCompleted))
                }
                upload.responseJSON { response in
                    if let dict = response.result.value as? NSDictionary{
                        let url = dict["url"] as! String
                        completion(url)
                    }
                }
            case .failure(let encodingError):
                print("-----",encodingError)
                
            }
        })
        
    }
    
    func getEvents(url: URL, headers: HTTPHeaders?, method: HTTPMethod?, parameters: [String: Any]?, completion: @escaping ([Event]?) -> ()){
        Alamofire.request(url, method: method!, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { response in
            
            switch response.result{
            case .success:
                if let responseString = response.result.value{
                    
                    if let eventApiResponse = Mapper<EventsApiResponse>().map(JSONString: responseString){
                        completion(eventApiResponse.events)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func addEvent(url: URL, headers: HTTPHeaders?, method: HTTPMethod?, parameters: [String: Any]?, completion: @escaping () -> ()){
        
        Alamofire.request(url, method: method!, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            switch response.result{
            case .success:
                if let result = response.result.value{
                    print(result)
                    completion()
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    func addFavEvent(id: String, headers: HTTPHeaders ,completion: @escaping (Bool)->()){
        guard let url = URL(string: favoriteEvent+id) else { return }
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result{
            case .success:
                if let result = response.result.value as? Dictionary<String, Any>{
                    guard let success = result["success"] as? Bool else{ return }
                    print(result["message"] as! String)
                    completion(success)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func deleteFavEvent(id: String, headers: HTTPHeaders ,completion: @escaping (Bool)->()){
        guard let url = URL(string: deleteFavoriteEvent+id) else { return }
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result{
            case .success:
                if let result = response.result.value as? Dictionary<String, Any>{
                    guard let success = result["success"] as? Bool else{ return }
                    print(result["message"] as! String)
                    completion(success)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}











