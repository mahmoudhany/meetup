//
//  TagCell.swift
//  Meetup
//
//  Created by Mahmoud on 7/22/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var tagLabelMaxWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        tagLabel.font = UIFont(name: "SourceSansPro-Regular", size: 18)
        tagLabel.textColor = .black
        self.backgroundColor = UIColor.rgb(red: 216, green: 216, blue: 216)
        self.layer.cornerRadius = 5
        self.tagLabelMaxWidthConstraint.constant = UIScreen.main.bounds.width - 8 * 2
    }
    
}








