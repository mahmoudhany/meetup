//
//  Validation.swift
//  Meetup
//
//  Created by Mahmoud on 4/18/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit


class Validation{
    
    static func isValidEmail(email: String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidName(name: String) -> Bool{
        let nameRegEx = "[a-zA-Z]+[a-zA-Z_ ]+$"
        
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: name)
    }
    static func isValidTime(time: String) -> Bool{
        let timeRegEx = "[A-Z0-9a-z._%+-]+[A-Z0-9a-z_: ]+$"
        
        let timeTest = NSPredicate(format:"SELF MATCHES %@", timeRegEx)
        return timeTest.evaluate(with: time)
    }
    
    static func isValidPassword(password: String) -> Bool{
        let passwordRegEx = "(?=.*[0-9]).{6,30}$"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
    
    static func validationAlert(title: String, message: String, actionTitle: String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .cancel, handler: nil)
        alert.addAction(action)
        return alert
    }
    
}
