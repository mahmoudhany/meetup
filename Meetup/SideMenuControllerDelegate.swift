//
//  SideMenuControllerDelegate.swift
//  Meetup
//
//  Created by Mahmoud on 9/14/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import Foundation

protocol SideMenuControllerDelegate {
    func didSelectCell(_ cell: MenuCell)
}
