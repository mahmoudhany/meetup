//
//  EventCell.swift
//  Meetup
//
//  Created by Mahmoud on 4/29/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

protocol ProfileEventCellDelegate{
    func didTapFavoriteButton(cell: EventCell)
}


class EventCell: UICollectionViewCell {
   
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    
    @IBOutlet weak var favoriteButton: UIButton!
    var isFavorited: Bool!
    
    var delegate: ProfileEventCellDelegate?
    
    let border: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.addSubview(border)
        border.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            border.heightAnchor.constraint(equalToConstant: 1),
            border.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            border.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            border.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
            ])
        
        favoriteButton.setImage(#imageLiteral(resourceName: "favorite-fill").withRenderingMode(.alwaysTemplate), for: .normal)
        favoriteButton.tintColor = mainGreenColor
    }
    @IBAction func favoriteButtonTapped(_ sender: Any) {
        delegate?.didTapFavoriteButton(cell: self)
    }
}











