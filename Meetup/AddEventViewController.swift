//
//  AddEventViewController.swift
//  Meetup
//
//  Created by Mahmoud on 7/2/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class AddEventViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var cityTextField: CustomTextField!
    @IBOutlet weak var countryTextField: CustomTextField!
    @IBOutlet weak var addressTextField: CustomTextField!
    @IBOutlet weak var dateTextField: CustomTextField!
    @IBOutlet weak var timeTextField: CustomTextField!
    @IBOutlet weak var tagsTextField: CustomTextField!
    @IBOutlet weak var detailsTextField: CustomTextField!
    var headers: HTTPHeaders?
    var tags: [[String: String]] = []
    var eventDate: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        handleTextFieldsDelegate()
        dismissKeyboard()
        textFieldClicked(textField: dateTextField)
        textFieldClicked(textField: timeTextField)
        
        checkLogin()
    }
    private func checkLogin(){
        guard let accessToken = KeychainWrapper.standard.string(forKey: "accessToken") else{
            return
        }
        headers = [
            "x-access-token": accessToken,
            "api-key": "VjcmyTB3WCzanmMxqDpzTQNBo"
        ]
    }
    func dismissKeyboard(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gesture:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    func handleTextFieldsDelegate(){
        nameTextField.delegate = self
        cityTextField.delegate = self
        countryTextField.delegate = self
        addressTextField.delegate = self
        dateTextField.delegate = self
        timeTextField.delegate = self
        detailsTextField.delegate = self
        tagsTextField.delegate = self
    }
    @IBAction func dismissAddEvent(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindToAddEventViewController(segue: UIStoryboardSegue) {
        let source = segue.source as? TagsViewController
        self.tags.removeAll()
        let titles = source?.selectedTagsTitles.map{ (title) -> String in
            return title
            }
        for title in titles!{
            let dict = ["value": title, "label": title]
            tags.append(dict)
        }
    
        if let title = titles?.joined(separator: ", "){
            self.tagsTextField.text = title
        }
    }
    
    @IBAction func addEventPressed(_ sender: Any) {
        guard
            let name = self.nameTextField.text,
            let city = self.cityTextField.text,
            let country = self.countryTextField.text,
            let address = self.addressTextField.text,
            let date = self.dateTextField.text,
            let time = self.timeTextField.text,
            let details = self.detailsTextField.text
            else{
                return
        }
        let tags = self.tags
        if Validation.isValidName(name: name) &&
            Validation.isValidName(name: city) &&
            Validation.isValidName(name: country) &&
            Validation.isValidName(name: address) &&
            Validation.isValidTime(time: time) &&
            Validation.isValidTime(time: date)
        {
            let parameters: [String: Any] = [
                "name": name,
                "city": city,
                "country": country,
                "address": address,
                "date": self.eventDate!,
                "time": time,
                "details": details,
                "tags": tags
            ]
            // submit new data
            ApiService.sharedInstance.addEvent(url: eventsUrl!, headers: headers, method: .post, parameters: parameters, completion: {
                self.dismiss(animated: true, completion: nil)
            })
        }else{
            let alert = Validation.validationAlert(title: "Failed", message: "Please fill all Fields correctly", actionTitle: "Dismiss")
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: textField handling
extension AddEventViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            cityTextField.becomeFirstResponder()
        case cityTextField:
            countryTextField.becomeFirstResponder()
        case countryTextField:
            addressTextField.becomeFirstResponder()
        case addressTextField:
            dateTextField.becomeFirstResponder()
        case dateTextField:
            timeTextField.becomeFirstResponder()
        case timeTextField:
            tagsTextField.becomeFirstResponder()
        case tagsTextField:
            detailsTextField.becomeFirstResponder()
        default:
            nameTextField.resignFirstResponder()
        }
        return true
    }
    
    @objc func viewTapped(gesture: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    func textFieldClicked(textField: UITextField){
        let datePicker = UIDatePicker()
        datePicker.setValue(UIColor.black, forKeyPath: "textColor")
        textField.inputView = datePicker
        textField.inputView?.backgroundColor = .white
        switch textField {
            
        case dateTextField:
            datePicker.datePickerMode = .date
            let currentDate = Date()
            datePicker.minimumDate = currentDate
            datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
            
        case timeTextField:
            datePicker.datePickerMode = .time
            datePicker.addTarget(self, action: #selector(timeChanged(timePicker:)), for: .valueChanged)
        default:
            break
        }
        
    }
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        
        let date = datePicker.date
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        eventDate = dateFormatter2.string(from: date)
    }
    
    @objc func timeChanged(timePicker: UIDatePicker) {
        let timeFormatter = DateFormatter()
        
        timeFormatter.timeStyle = .short
        timeTextField.text = timeFormatter.string(from: timePicker.date)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tagsTextField{
            performSegue(withIdentifier: "toTagsVC", sender: self)
            return false
        }
        return true
    }
}




